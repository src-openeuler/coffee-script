%global commit f26d33d418dcdcfcc6ad3ab774d9cabbf7af659c
%global shortcommit %(c=%{commit}; echo ${c:0:7})
Name:                coffee-script
Version:             1.10.0
Release:             1
Summary:             A programming language that transcompiles to JavaScript
License:             MIT
URL:                 http://coffeescript.org/
Source0:             https://github.com/jashkenas/coffee-script/archive/%{commit}/coffee-script-%{version}-%{shortcommit}.tar.gz
ExclusiveArch:       %{nodejs_arches} noarch
Patch1:              coffee-script-Cakefile.patch
BuildRequires:       nodejs-packaging web-assets-devel npm(underscore) uglify-js
Requires:            js-%{name} == %{version}-%{release}
BuildArch:           noarch

%description
CoffeeScript is a little language that compiles into JavaScript. Underneath all
of those embarrassing braces and semicolons, JavaScript has always had a
gorgeous object model at its heart. CoffeeScript is an attempt to expose the
good parts of JavaScript in a simple way.
The golden rule of CoffeeScript is: "It's just JavaScript". The code compiles
one-to-one into the equivalent JS, and there is no interpretation at runtime.
You can use any existing JavaScript library seamlessly (and vice-versa). The
compiled output is readable and pretty-printed, passes through JavaScript Lint
without warnings, will work in every JavaScript implementation, and tends to run
as fast or faster than the equivalent handwritten JavaScript.

%package -n js-%{name}
Summary:             A programming that transcompiles to JavaScript - core compiler
Provides:            %{name}-common = %{version}-%{release}
Obsoletes:           %{name}-common < 1.10.0-4
Requires:            web-assets-filesystem

%description -n js-%{name}
This is the core compiler for the CoffeeScript language, suitable for use in
browsers or by other JavaScript implementations.
For the primary compiler and cake utility used in conjunction with Node.js,
install the 'coffee-script' package.

%package doc
Summary:             A programming language that transcompiles to JavaScript - documentation

%description doc
The documentation for the CoffeeScript programming language.

%prep
%setup -qn coffeescript-%{commit}
%patch1 -p1
mv documentation html

%build
%nodejs_symlink_deps --build
./bin/cake build
./bin/cake build:browser
mv extras/coffee-script.js extras/coffee-script.min.js
MINIFY=false ./bin/cake build:browser

%install
mkdir -p %{buildroot}%{_jsdir}/%{name}/
cp -pr extras/* %{buildroot}%{_jsdir}/%{name}/
mkdir -p %{buildroot}%{_datadir}/%{name}/extras/
ln -s %{_jsdir}/%{name}/coffee-script.js %{buildroot}%{_datadir}/%{name}/extras/
ln -s %{_jsdir}/%{name}/coffee-script.min.js %{buildroot}%{_datadir}/%{name}/extras/
mkdir -p %{buildroot}%{nodejs_sitelib}/%{name}
cp -pr *.js bin lib package.json %{buildroot}%{nodejs_sitelib}/%{name}
chmod 0644 %{buildroot}%{nodejs_sitelib}/%{name}/lib/coffee-script/parser.js
ln -sf %{_datadir}/%{name}/extras %{buildroot}%{nodejs_sitelib}/%{name}/extras
mkdir -p %{buildroot}%{_bindir}
ln -sf ../lib/node_modules/%{name}/bin/coffee %{buildroot}%{_bindir}/coffee
ln -sf ../lib/node_modules/%{name}/bin/cake %{buildroot}%{_bindir}/cake

%check
./bin/cake test

%pretrans -p <lua>
path = "%{nodejs_sitelib}/%{name}/lib"
st = posix.stat(path)
if st and st.type == "link" then
  os.remove(path)
end

%files
%{nodejs_sitelib}/%{name}
%{_bindir}/coffee
%{_bindir}/cake

%files -n js-%{name}
%doc README.md
%license LICENSE
%{_jsdir}/%{name}
%{_datadir}/%{name}

%files doc
%doc html

%changelog
* Fri Aug 14 2020 leiju <leiju4@huawei.com> - 1.10.0-1
- Package init
